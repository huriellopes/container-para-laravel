# Container Docker

> Container docker para Laravel, com integração:
    - PHP 7.2.14
    - Nginx
    - Redis
    - Mysql 5.7

## Alterações

> Alterar as seguintes coisas:

- Criar a pasta "mysql/tmp"
    Ex.: 
    volumes:
      - ./mysql/tmp:/var/lib/mysql

- Criar a pasta "redis/tmp"
    Ex.:
    volumes:
      - ./redis/tmp:/var/lib/redis

- mudar o diretório do projeto em php
    Ex.:
    volumes:
      - ./app:/usr/share/nginx/html
- mudar o diretório do projeto em web
    Ex.:
    volumes:
      - ./app:/usr/share/nginx/html
      - ./docker/nginx/default.conf:/etc/nginx/conf.d/default.conf

## Credits

Huriel Lopes - Desenvolvedor Web